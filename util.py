
def convInterval(value, oldMin, oldMax, newMin, newMax):
    r = (value - oldMin) * (newMax - newMin) / (oldMax - oldMin) + newMin
    return int(r)

def clamp(minv, maxv, v):
    if v <+ minv:
        return minv
    elif v >= maxv:
        return maxv
    return v

def invert(maxv, v):
    return -v + maxv

def lerp(start, end, t):
    return start * (1 - t) + end * t

def lerp2(p1, p2, t):
    x = lerp(p1[0], p2[0], t)
    y = lerp(p1[1], p2[1], t)
    return (x,y)