import math


# get magnitude of vector
def magnitude(p):
    return(math.sqrt(p[0]**2 + p[1]**2))

# translate vector by realative amount
def translate(p, d):
    return(p[0]+d[0], p[1]+d[1])

# normalize vector
def normalize(p):
    mag = magnitude(p)
    if mag != 0:
        return((p[0]/mag, p[1]/mag))
    return((0, 0))

# scale vector by given scalar
def scale(p, scalar):
    return((p[0] * scalar, p[1] * scalar))

def add_magnitude(p, to_add):
    original_magnitude = magnitude(p)
    norm = normalize(p)
    return norm.scale(to_add + original_magnitude)

def add(p1, p2):
    return (p1[0]+p2[0],p1[1]+p2[1])

def set_angle(p, angle):
    x = math.cos(angle)
    y = math.sin(angle)
    #print (x,y)
    return scale((x,y), magnitude(p))

def perpendicular_left(p):
    return (-p[1], p[0])

def perpendicular_right(p):
    return (p[1],-p[0])

def inverse(p):
    return (-p[0],-p[1])

def get_angle(p):
    return math.atan2(p[0],p[1])

# gets midpoint between two vectors
#def midpoint(p1, p2):
#    d1 = ((p1[0]+p2[0])/2, (p1[1]+p2[1])/2)
#    return normalize(d1)

# divide vectors
#def divide(p1, p2):
#    return (p1[0]/p2[0], p1[1]/p2[1])

# substract vectors
#def subtract(p1, p2):
#    return (p1[0]-p2[0], p1[1]-p2[1])

# divide vector by scalar
#def divide_value(p, scalar):
#    return (p[0]-scalar, p[1]-scalar)
