import pygame

import math

import vector_util as vector
import march
import util



#def draw_camera(target, camera_pos, screen_width, screen_distance, simple=False):
#    colour = (255, 128, 0)
#    hud_offset = (300,400)
#    screen_halfwidth = int(screen_width/2)
#    if not simple:
#        for w in range(-screen_halfwidth, screen_halfwidth+1, 1):
#            rem = (camera_pos[0]+w, camera_pos[1]+screen_distance)
#            pygame.draw.line(target, colour, vector.translate(camera_pos, hud_offset), vector.translate(rem, hud_offset))
#    else:
#        pygame.draw.circle(target, colour, vector.translate(camera_pos, hud_offset), 8)
#        pygame.draw.circle(target, colour, vector.translate((camera_pos[0], camera_pos[1]+screen_distance), hud_offset), 4)
#
#    colour = (128, 128, 128)
#    pygame.draw.circle(target, colour, vector.translate((0, 30), hud_offset), 32)

#def draw_world(target, camera_pos, screen_width, screen_distance):
#    line = march.render_line(camera_pos=camera_pos, screen_width=screen_width, screen_distance=screen_distance)
#    for i, v in enumerate(line):
#        c = util.clamp(0, 255, util.invert(255, v))
#        c = util.clamp(0, 255, util.convInterval(v, 0, 1024, 255, 0))
#        colour = (c, c, c)
#        pygame.draw.rect(target, colour, (i, 200, 1, 64))
#        pass
#    print(line)


def draw_camera(target, line_data):
    hud_offset = (300,400)
    colour = (0, 255, 0)
    for i, v in enumerate(line_data):
        for pi, pv in enumerate(v["path"]):
            if pi == 0:
                pygame.draw.circle(target, colour, vector.translate((pv[0], pv[1]), hud_offset), 4)
                pygame.draw.circle(target, (255,0,0), vector.translate(v["remote"], hud_offset), 2)
            else:
                pygame.draw.circle(target, colour, vector.translate((pv[0], pv[1]), hud_offset), 1)

    pass

def draw_world(target, camera_pos, camera_angle, screen_width, screen_height, screen_distance, texture):
    #draw_camera(target, camera_pos, screen_width, screen_distance)
    c = 255
    colour = (c, c, c)
    line_data = march.render_line(camera_pos, screen_width, screen_distance, camera_angle)
    tl = []
    for i, v in enumerate(line_data):
        tl.append(["_","X"][v["hit"]])
        if v["hit"]:
            dist = v["perp"]
            # determine the colour of the wall as a function of the distance
            #d = util.clamp(0, 255,  dist * 16 - 600)
            #c = util.invert(255, d)
            c = 255
            colour = v["color"]
            # determine the height of each scanline as a function of the distance
            height = int((screen_height/dist)*4)
            top = (screen_height/2)-(height/2)
            horizontal_uv = math.floor(colour[0]/255.0*texture.get_width())
            texture_rect = (horizontal_uv, 0, 1, texture.get_height())
            scaled = pygame.transform.scale(texture, (texture.get_width(), height))
            target.blit(scaled, (i, top, 1, height), (horizontal_uv, 0, 1, height))
            #for vertical in range(math.ceil(vertical_divisions)):

                #vertical_uv = math.floor(vertical)

                #print(vertical)
                #texture.blit()
                #thisPixel = texture.get_at((horizontal_uv, vertical_uv))
                #pygame.draw.rect(target, thisPixel, (i, top + (vertical * pixels_per_division), 1, pixels_per_division+2))
            #pygame.draw.rect(target, colour, (i, 0, 1, 600))
    #print(tl)
    draw_camera(target, line_data)





def main():
    pygame.init()
    pygame.display.set_caption("Yes, rays")
    screen = pygame.display.set_mode((800, 600))

    colour = (128,128,128)
    #speed = 0.5
    speed = 1
    texture = pygame.image.load("test_text.png")
    camera_pos = [0, -100]
    camera_angle = 0
    screen_width = 800
    #screen_width = 40
    #screen_width = 800
    #screen_halfwidth = int(screen_width/2)
    #screen_distance = 20
    #screen_distance = 40
    screen_distance = 64
    #screen_distance = 100
    #screen_distance = 400

    clock = pygame.time.Clock()

    #surf = pygame.Surface((32*8, 32*8))
    pygame.key.set_repeat(1, 10)

    running = True



    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w:
                    forward = vector.set_angle((0,speed),camera_angle)
                    camera_pos = vector.add(camera_pos, forward)
                elif event.key == pygame.K_s:
                    back = vector.inverse(vector.set_angle((0,speed),camera_angle))
                    camera_pos = vector.add(camera_pos, back)
                elif event.key == pygame.K_a:
                    left = vector.perpendicular_right(vector.set_angle((0,speed),camera_angle))
                    camera_pos = vector.add(camera_pos, left)
                elif event.key == pygame.K_d:
                    right = vector.perpendicular_left(vector.set_angle((0,speed),camera_angle))
                    camera_pos = vector.add(camera_pos, right)
                elif event.key == pygame.K_LEFT:
                    camera_angle -= math.radians(1)
                elif event.key == pygame.K_RIGHT:
                    camera_angle += math.radians(1)

        screen.fill((0, 0, 0))



        #draw_world(screen, camera_pos, screen_width, screen_distance)
        draw_world(screen, camera_pos, camera_angle, screen_width, 600, screen_distance, texture)
        #draw_camera(screen, camera_pos, screen_width, screen_distance, simple=True)
        #draw_camera(screen, camera_pos, screen_width, screen_distance)


        pygame.display.flip()





if __name__ == "__main__":
    main()

#        screen.fill((8, 64, 128))
#        #draw_board(surf)
#        #screen.blit(surf, (0,0))
#        #line = march.render_line(camera_pos=(player_x, player_y), screen_width=800)
#        line = march.render_line(camera_pos=(player_x, player_y), screen_width=800)
#        #print(line)
#        for i, v in enumerate(line):
#            #print(i)
#            c = clamp(0, 255, convInterval(v, 0, 1000, 255, 0))
#            #c = clamp(0, 255, convInterval(v, 0, 10, 255, 0))
#            #print(c)
#            #c = invert(255, clamp(0, 255, v))
#            colour = (c, c, c)
#            #print(colour)
#
#            pygame.draw.rect(screen, colour, (i, 0, 1, 600))
#
#        print((player_x, player_y))
#        pygame.display.flip()
#        clock.tick(60)
